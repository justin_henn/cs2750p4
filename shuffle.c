//shuffle.c: Shuffle cards 
//This program shuffles a deck of cards by swapping the first card with a random card. It also uses qsort to sort cards by number
//Author: Justin Henn
//Date 4/4/2016
//Assignment: 4
#include <stdio.h>
#include <stdlib.h>


//definitions for card structure and enum

  typedef enum { clubs, diamonds, hearts, spades} suit_t;
  typedef struct {

    unsigned int number  : 4;
    suit_t       suit    : 2;
  } card_t;

//function used for qsort to compare card numbers and sort

int compare ( const void * a, const void * b) {

  card_t *card_t_a = (card_t*)a;
  card_t *card_t_b = (card_t*)b;
  return (card_t_a->number > card_t_b->number);


}

//function to print the actual card number of the card

void find_number(int num) {

  switch(num) {

    case 0:
      printf ("Ace of ");
      break;
    case 1:
      printf ("2 of ");
      break;
    case 2:
      printf ("3 of  ");
      break;
    case 3:
      printf ("4 of ");
      break;
    case 4:
      printf ("5 of ");
      break;
    case 5:
      printf ("6 of ");
      break;
    case 6:
      printf ("7 of ");
      break;
    case 7:
      printf ("8 of ");
      break;
    case 8:
      printf ("9 of ");
      break;
    case 9:
      printf ("10 of ");
      break;
    case 10:
      printf ("Jack of ");
      break;
    case 11:
      printf ("Queen of ");
      break;
    case 12:
      printf ("King of ");
      break;

  }
}

//function that calls find_number to print the card number then finds the suit of the card and prints it

void print_it (card_t decks[]) {

  int x;
  for (x = 0; x < 52; x++) {
    
    find_number( decks[x].number);
    switch(decks[x].suit) {
      case 0:
	printf ("Clubs\n");
	break; 
      case 1:
	printf ("Diamonds\n");
	break;
      case 2:
	printf ("Hearts\n");
	break;
      case 3:
	printf ("Spades\n");
	break;
  
    }
  }
}

int main() {


  time_t t;
  srand((unsigned) time(&t));
  int i;
  card_t deck[52];
  card_t tmp;

  int y = 0, z = 0, x = 0;

  //loop that fills that card deck with cards in order

  for (x = 0; x < 52; x++) {

    deck[x].number = y;
    deck[x].suit = z;

    if (y == 12) {

      y = 0;
      z++;
    }
    else
      y++;
  }

  //loop that shuffles cards by exchanging the card at the first slot with a card that picked by a random number

  for( i = 0 ; i < 52 ; i++ ) {
	    
   // printf("%d\n", rand() % 52);
   int ran_num = (rand() % 52);

    tmp = deck[0];
    deck[0] = deck[ran_num];
    deck[ran_num] = tmp;


  }

  //print the deck
  
  printf("Printing swapped deck of cards:\n");
  print_it(deck);

  printf("\n");


  //use qsort to sort deck by number and print it

  qsort(deck, 52, sizeof(card_t), compare);


  printf("Printing qsorted deck of cards:\n");

  print_it(deck);

  return (0);
}
