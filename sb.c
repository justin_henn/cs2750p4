//sb.c: Shuffle deck of cards
//This program uses a bit vector to shuffle cards randomaly. It then uses a sort to sort the cards by suit.
//Author: Justin Henn
//Date: 4/4/2016
//Assignment: 4
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

//structure and enum definitions

typedef enum { clubs, diamonds, hearts, spades} suit_t;
typedef struct {

  unsigned int number  : 4;
  suit_t       suit    : 2;
} card_t;

//function to print the actual card number


void find_number(int num) {

  switch(num) {
        
    case 0: 
      printf ("Ace of ");
      break;
    case 1: 
      printf ("2 of ");
      break;
    case 2:
      printf ("3 of  ");
      break;
    case 3:
      printf ("4 of ");
      break;
    case 4:
      printf ("5 of ");
      break;
    case 5:
      printf ("6 of ");
      break;
    case 6:
      printf ("7 of ");
      break;
    case 7:
      printf ("8 of ");
      break;
    case 8:
      printf ("9 of ");
      break;
    case 9:
      printf ("10 of ");
      break;
    case 10:
      printf ("Jack of ");
      break;
    case 11:
      printf ("Queen of ");
      break;
    case 12:
      printf ("King of ");
      break;
  }
}

//function that calls the find_number function to print the card number and the finds the suit and prints it

void print_it (card_t decks[]) {

    
  int x;

  for (x = 0; x < 52; x++) {

   find_number(decks[x].number);
    switch(decks[x].suit) {
      case 0:
	printf ("Clubs\n");
	break;
      case 1:
	printf ("Diamonds\n");
	break;
      case 2:
	printf ("Hearts\n");
	break;
      case 3:
	printf ("Spades\n");
	break;

    }
  }
}

//function that is used by qsort to compare the card suits and sort

int compare (const void * a, const void * b) {

  card_t *card_t_a = (card_t*)a;
  card_t *card_t_b = (card_t*)b;
  return (card_t_a->suit > card_t_b->suit);

}

int main() {

  unsigned char deck[7];
  memset(deck, 0, sizeof(deck));
  card_t deck_cards[52];

  int i, ran_num, iter, iter1;

  time_t t;
  srand((unsigned) time(&t));

  //loop that takes a random number and checks the bit vector to see if the card is not already in the deck
  //if the card is used then it finds another random number until it finds a card that is not used and then
  //adds that card to the deck and updates the bit vector with a 1 at the position needed

  for( i = 0 ; i < 52 ; i++ ) {

    ran_num = (rand() % 52);
    iter1 = ran_num / 8;
    iter = ran_num % 8;
    while ((deck[iter1] & (1 << iter)) != 0){
    
/*((deck[iter1] >> iter) & 1)*/
      ran_num = (rand() % 52);
      iter1 = ran_num / 8;
      iter = ran_num % 8;
    }
    deck[iter1] |= 1 << iter;
    deck_cards[i].number = ran_num % 13;
    deck_cards[i].suit = ran_num / 13;
    
  }

  //print the deck as is

  printf("Printing bit vector sorted deck of cards:\n");
  print_it(deck_cards);
  

  printf("\n");

  //sort the deck and print it

  qsort(deck_cards, 52, sizeof(card_t), compare); 
  printf("Printing qsorted deck of cards:\n");
  print_it(deck_cards);

  return ( 0 );
}
