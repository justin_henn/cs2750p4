//regex_magic.c: Search account info
//Searches account information by account number or regex
//Author: Justin Henn
//Date: 4-4-15
//Assignment: 4


#include <stdio.h>
#include <ctype.h>
#include <regex.h>

//definition of record structure

typedef struct
{
  char name[40];
  int number;
  float balance;
} acct_info_t;

//function that asks if you want to search again

char go_again() {

  char a;
  printf("Do you want to search(y/n)\n");

  scanf(" %c", &a);
  return a;

}


/*IGNORE ALL OF THIS
 


//function that changes endian of balance

float reverse_float(float in_float) {

  float return_value;
  char *float_convert = ( char* ) & in_float;
  char *return_float = ( char* ) & return_value;

  return_float[0] = float_convert[3];
  return_float[1] = float_convert[2];
  return_float[2] = float_convert[1];
  return_float[3] = float_convert[0];

  return return_value;

}

//function that changes endian of unsigned int

unsigned int byte_swap(unsigned int a) {

  a = ((a << 8) & 0xFF00FF00 ) | ((a >> 8) & 0xFF00FF ); 

  return (a << 16) | (a >> 16);

}

END IGNORE */

int main () {

  FILE * fd;
  unsigned int magic;
  unsigned int num_rec;
  unsigned int test = 0x61636374, check = 0x80808080;
  acct_info_t acct;
  acct.number = 10;
  int another_rec;
  char line[80];

  //open file
 
  if((fd = fopen("acct_info", "r")) == NULL) {
  
    printf( "Error opening file\n");
    return ( 1 );
  
  }

  //read maginc number and number of records
  //and verifies magic number is correct


  fread(&magic, sizeof(unsigned int), 1, fd);
  fread(&num_rec, sizeof(unsigned int), 1, fd);
  magic = magic - check;
  unsigned int swap_magic = magic; 
  unsigned int swap_rec = num_rec; 
  if(test != swap_magic) {
  
    printf("Magic number is not correct\n");
    return ( 1 );  
  }

  int r;
  regex_t reg;
  regmatch_t match[1];
  int ig_case;

  //begin search

  char answer = go_again();
  int search_type;
  if (answer == 'n') {
    fclose(fd);
    return (0);
  }

  //loop that keeps asking if you want to search and does search if you do

  while (answer != 'n') {
  
    printf("Regular expression or account number search(1 for regex, 2 for account number)\n");
    scanf(" %d%*c", &search_type);

    //regular expression search

    if (search_type == 1) {
        printf("Input a regular expression: ");
  	scanf(" %s", &line);

	printf("Ignore Case? (1 for yes, 2 for no): ");
	scanf("%d", &ig_case);

	if (ig_case == 1)
	  regcomp(&reg, line, REG_ICASE | REG_EXTENDED);

	else if (ig_case == 2)
	  regcomp(&reg, line, REG_EXTENDED);

	fseek(fd, 2*(sizeof(unsigned int)), SEEK_SET);
  
	while ( ! feof ( fd ) ) {
  
  	  fread(&acct, sizeof(acct_info_t), 1, fd);
  
	  if (feof ( fd ) )
	    break;

	  r = regexec(&reg, acct.name, 1, match, 0);
 
	  if(r == 0)
	    printf("Account Number: %-5d Name: %-40s Balance: $%6.2f\n", acct.number, acct.name,acct.balance);
	}
    }

    //account number search

    else {
      
      printf("Insert account number between 1-%u:", swap_rec);
      fgets(line, sizeof(line), stdin);
      //scanf(line, "%d", &another_rec);
      if (!(isdigit(line[0]))) {

	printf("Not a Number.\n");
	break;

      }
      sscanf(line, "%d", &another_rec);

      if (another_rec > (int)swap_rec) {

	printf("Not a valid record number.\n");
	break;

      }

      fseek (fd, ((another_rec - (int)swap_rec)-1)*sizeof(acct_info_t), SEEK_END);
      fread(&acct, sizeof(acct_info_t), 1, fd);
      printf("Account Number: %-5d Name: %-35s Balance: $%6.2f\n", acct.number, acct.name, acct.balance);
  }

    //ask to search again

    answer = go_again();

    if (answer == 'n')
      break;

  }
  
  //close file

  fclose(fd);
  return ( 0 );

}
