//magic_number.c: Search account info
//This program searches accounts by account number
//Author: Justin Henn
//Date 4/4/2016
//Assignment: 4

#include <stdio.h>
#include <ctype.h>

//structure for an acct in the binary file
typedef struct
{
  char name[40];
  int number;
  float balance;
} acct_info_t;


/*****************PLEASE IGNORE THIS CODE **********************


//change the bits for the balance so it is big endian instead of little endian

float reverse_float(float in_float) {

  float return_value;
  char *float_convert = ( char* ) & in_float;
  char *return_float = ( char* ) & return_value;
  return_float[0] = float_convert[3];
  return_float[1] = float_convert[2];
  return_float[2] = float_convert[1];
  return_float[3] = float_convert[0];
 
  return return_value;

}

//changes the bits for the record numbers and the magic number so big endian instead of little endian

unsigned int byte_swap(unsigned int a) {

  a = ((a << 8) & 0xFF00FF00 ) | ((a >> 8) & 0xFF00FF ); 

  return (a << 16) | (a >> 16);

}

****************END IGNORE ************************/

//main code

int main () {

  FILE * fd;
  unsigned int magic;
  unsigned int num_rec;
  unsigned int test = 0x61636374, check = 0x80808080;
  acct_info_t acct;
  acct.number = 10;
  int another_rec;
  char line[80];

  //open file
  if((fd = fopen("acct_info", "r")) == NULL) {
  
    printf( "Error opening file\n");
    return ( 1 );
  
  }

  //read in the magic number and number of records

  fread(&magic, sizeof(unsigned int), 1, fd);
  fread(&num_rec, sizeof(unsigned int), 1, fd);
  magic = magic - check;
  unsigned int swap_magic = magic; 
  unsigned int swap_rec = num_rec; 
  
  //check to see if the magic number matches what it should be  
  
  if(test != swap_magic) {
  
    printf("Magic number is not correct\n");
    return ( 1 );  
  }


  //get what record the user wants to look up and verify it is a digit

    printf("Insert account number between 1-%u (0 to quit): ", swap_rec);
    fgets(line, sizeof(line), stdin);
    if (isdigit(line[0]) == 0) {
    
      printf("Not a Number.\n");
      return (1);
    
    }
    sscanf(line, "%d", &another_rec);

 //loop that goes through and looks up the record depending on what record the user requested.

  while (another_rec != 0) {
 
    if (another_rec > (int)swap_rec) {

      printf("Not a valid record number.\n");
      return ( 1 );
    }
  

    fseek (fd, ((another_rec - (int)swap_rec-1))*sizeof(acct_info_t), SEEK_END); 
    fread(&acct, sizeof(acct_info_t), 1, fd);
    printf("Account Number: %-6d Name: %-35s Balance: $%6.2f\n", acct.number, acct.name, acct.balance);
    printf("Insert account number between 1-%u (0 to quit):\n", swap_rec);
    fgets(line, sizeof(line), stdin);
    sscanf(line, "%d", &another_rec);

    if (isdigit(line[0]) == 0) {
    
      printf("Not a Number.\n");
      return (1);
    
    }


  };

fclose(fd);
return ( 0 );

}
